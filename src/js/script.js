const menuToggle = document.querySelector(".navigation__dropdown--btn");
const menuDropdown = document.querySelector (".navigation__dropdown");
const menuNavigation = document.querySelector (".navigation__mobnav");
menuToggle.onclick = function () {
    menuDropdown.classList.toggle('active');
    menuToggle.classList.toggle('active');
    menuNavigation.classList.toggle('active');
}
